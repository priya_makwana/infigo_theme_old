{
    # Theme information
    'name' : 'Infigo Category',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Website Product Category Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_carousel'
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
