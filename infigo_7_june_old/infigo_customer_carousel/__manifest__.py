{
    # Theme information
    'name' : 'Infigo Customer Carousel',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Showcase your Customers using Infigo Customer Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_carousel','infigo_business_snippets'
    ],

    # Views
    'data': [
         'templates/assets.xml',
         'templates/customer_carousel_snippet.xml',
         'templates/customer_carousel_snippent_option.xml',
         'view/res_partner.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
