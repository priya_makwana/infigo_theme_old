{
    # Theme information
    'name' : 'Infigo Business Snippets',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'All Custom CMS Block Builders',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_layout'
    ],

    # Views
    'data': [
        'templates/snippets.xml', 
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
