
$(document).ready(function()
{
	/*view filter click */
	$(".apply-filter").click(function(){
		//$("#products_grid_before").css({"width":"0px","transition":"0.5s"});
		//$("#wrapwrap").css({"margin-left":"0px","transition":"0.5s"});
		$(".main-header-maxW").removeClass("transparent");
		$(".filter-main-div").css("display","block").addClass("zoom-fadein");
	});
	/*close button of view filter*/
	$(".filter-option-close-btn").click(function(){
		$(".filter-main-div").css("display","none");
	})
	
	/* filter-option class set center of screen */
	var winH = $(window).height();
	var winW = $(window).width();
	var $filter_option = $(".filter-option");
	$filter_option.css('top',  winH/2-$filter_option.height()/2);
	$filter_option.css('left', winW/2-$filter_option.width()/2);
	
	$("form.js_attributes input").each(function(){ 
		var curr=$(this)        	
		var curr_parent=curr.parent();        	
		var status=this.checked  
		var val=this.value;
		if(status==true){ 
			$(".no-any-variant").css("display","none");
			$(".clear-all-filter").css("display","block");
			$(".menu_view_filter_div").removeClass("hidden")
			var curr_att=curr_parent.find("label").html() 
			if(curr_att){        		
				$("#pull-out").append("<div class='attribute'>" + curr_att + "<a data-id='"+val+"' class='close-btn'>X</a> </div>")        	
			}
			if(!curr_att){        			
				val_name=$(this).attr("title")        			
				$("#pull-out").append("<div class='attribute'>" + val_name + "<a data-id='"+val+"' class='close-btn'>X</a> </div>")        		
			}
		}
	});
	
	// Pricefilter Attribute
	var p_filter = ""
	if($("input[name='min_val']").val())
	{
		p_filter = $("input[name='min_val']").val() 
		if($("input[name='max_val']").val())
			{
			p_filter += "-"+$("input[name='max_val']").val()
			$(".no-any-variant").css("display","none");
			$(".clear-all-filter").css("display","block");
			$(".menu_view_filter_div").removeClass("hidden")
			$("#pull-out").append("<div class='attribute'>" + p_filter + "<a data-id='price' class='close-btn'>X</a> </div>")
			}
	}
		
	/* clear particular selected attribute */
	$(".close-btn").click(function(){        			
		var id=$(this).attr("data-id")        			
		if(id){
			if(id == 'price')
			{
				$("input[name='min_val']").val("")
				$("input[name='max_val']").val("")
				$("form.js_attributes input").closest("form").submit();
			}
			else
			{
			$("form.js_attributes input[value="+id+"]").removeAttr("checked");        			
			$("form.js_attributes input").closest("form").submit();
			}
			
		}
		if(!id){        				
			var id=$(this).attr("data-value")        				
			$("form.js_attributes input[value="+id+"]").removeAttr("checked");        				
			$("form.js_attributes input").closest("form").submit();        			
		}
	});
	/*Clear all selected attribute*/
	$(".clear-all-filter").click(function(){
		$("form.js_attributes input").each(function(){     	
			var curr=$(this)
			var status=this.checked
			if(status==true)  {
			curr.removeAttr("checked");
			}
		});
		$("input[name='min_val']").val("")
		$("input[name='max_val']").val("")
		$(".clear-all-filter").css("display","none");
		$(".menu_view_filter_div").addClass("hidden")
		$("form.js_attributes input").closest("form").submit();
		$(".no-any-variant").css("display","block");
	});
});

				
$(window).load(function(){
	$("form.js_attributes input").each(function(){ 
		var curr=$(this)        	
		var curr_parent=curr.parent();        	
		var status=this.checked  
		var val=this.value;
		if(status==true){
			$(".menu_view_filter_div").css("display","block");
			if("#filter_option_2"){
				$("#filter_option_2").parents(".view-as-maxW").find(".filter_option_style_2_div").css("display","inline-block");
			}
		}else if($("input[name='min_val']").val()){
			if("#filter_option_2"){
				$("#filter_option_2").parents(".view-as-maxW").find(".filter_option_style_2_div").css("display","inline-block");
			}	
		}
		else{
			$(".menu_view_filter_div").css("display","none;");
			$(".filter_option_2").css("display","none");
		}
	});
	if("#filter_option_2"){
		$(".filter_option_style_2_div").click(function(){
			$("#filter_option_2").toggle( "slow");
		})
	}
});
