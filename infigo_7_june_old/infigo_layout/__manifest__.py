{
    # Theme information
    'name' : 'Infigo Layout',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Navigate Online Store with Unique Mega Menu Styles',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_base'
    ],

    # Views
    'data': [
        'template/template.xml',
        'template/assets.xml',
        'view/theme_header.xml',
        'view/website_product_category.xml',
        'view/header_style.xml',
        'view/res_lang.xml',
        'data/data.xml',
        
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
