from odoo import fields,models
from docutils.nodes import field

class res_config_settings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    infigo_header_style_one = fields.Char('infigo header style1',related='website_id.infigo_header_style_one')
    infigo_header_style_one_image=fields.Binary("infigo header style one image",related='website_id.infigo_header_style_one_image')
    infigo_header_style_two = fields.Char("infigo header style2",related='website_id.infigo_header_style_two')
    infigo_header_style_two_image=fields.Binary("infigo header style two image",related='website_id.infigo_header_style_two_image')
