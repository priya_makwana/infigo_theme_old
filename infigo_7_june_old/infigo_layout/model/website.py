from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.http import request
 
class website(models.Model):
     
    _inherit = "website"
    
    infigo_header_style_one = fields.Char('infigo header style1')
    infigo_header_style_one_image=fields.Binary("infigo header style one image")
    infigo_header_style_two = fields.Char("infigo header style2")
    infigo_header_style_two_image=fields.Binary("infigo header style two image")
     
    def category_check(self,filter=[]):
         
        if filter:
            filter.extend([('is_website_published','=',True)])
        else:
            filter=([('is_website_published','=',True)])
         
        return self.env['product.public.category'].sudo().search(filter)
       
    def get_res_lang(self):
        
        current_lang= request.env.lang
        
        res_lang = request.env['res.lang'].search([('code','=',current_lang)])
        
    def check_typeof(self, var):
        var_type = str(type(var)).split("'")[1]
        return var_type
