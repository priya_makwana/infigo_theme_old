import json
import logging
from werkzeug.exceptions import Forbidden

from odoo import http, tools,_
from odoo.http import request
from odoo.addons.base.ir.ir_qweb.fields import nl2br
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.infigo_shop.controllers.main import infigoShop

class infigoNavigation(WebsiteSale):
    
    def getProduct(self,product,products):
        current_product_id=product.id
        totalele = len(products)-1
        try :
            curr_index = products.index(current_product_id)
            if (curr_index == 0) and (totalele > 0):
                return (False,request.env['product.template'].search([('id','=',products[curr_index+1])])) 
            elif (curr_index == 0) and (totalele == 0):
                return (False,False)
            elif curr_index == totalele:
                return (request.env['product.template'].search([('id','=',products[curr_index-1])]),False) 
            else:
                return (request.env['product.template'].search([('id','=',products[curr_index-1])]),request.env['product.template'].search([('id','=',products[curr_index+1])]))
        except ValueError:
            return (False,False)
            
    @http.route(['/shop/product/<model("product.template"):product>'], type='http', auth="public", website=True)
    def product(self, product, category='', search='',ept=None,**post):
        response = super(infigoNavigation, self).product(product=product, category=category, search=search, **post)            
        if ept:
            product_ids = request.session.get('product_ids', False)
            if product_ids:
                response.qcontext['ept'] = True
            elif product.public_categ_ids:
                categ = product.public_categ_ids[0]
                product_ids = request.env['product.template'].search([('public_categ_ids', 'child_of',int(categ.id))]).ids 
            else:
                product_ids = request.env['product.template'].search([]).ids   
        
        elif product.public_categ_ids:
            categ = product.public_categ_ids[0]
            product_ids = request.env['product.template'].search([('public_categ_ids', 'child_of',int(categ.id))]).ids
        else:
            product_ids = request.env['product.template'].search([]).ids
        if product_ids :
            prv_prd,next_prd=self.getProduct(product,product_ids)
            if next_prd:
                response.qcontext['next_prd'] = next_prd
            if prv_prd:
                response.qcontext['prv_prd'] = prv_prd
        return response

class infigoNavigationBar(infigoShop):
    # From Shop 
    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        response = super(infigoNavigationBar, self).shop(page=page, category=category, search=search,ppg=ppg, **post)
        all_product = response.qcontext.get('all_product')
        product_ids = request.session.get('product_ids', False)
        request.session['product_ids'] = None
        request.session['product_ids'] = all_product.ids
        return response
        
