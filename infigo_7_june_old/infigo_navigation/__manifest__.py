{
    # Theme information
    'name' : 'Infigo Product Navigation',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'View Next & Previous Items from Product Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_product'
    ],

    # Views
    'data': [
        'templates/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
