{
    # Theme information
    'name' : 'Infigo Ecommmerce CMS Blocks',
    'category' : 'Website',
    'version' : '1.0',
    'summary': '17 CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_snippet_style_1',
        'infigo_snippet_style_2',
        'infigo_snippet_style_3',
        'infigo_snippet_style_5',
        'infigo_snippet_style_6',
        'infigo_snippet_style_7',
        'infigo_snippet_style_8',
        'infigo_snippet_style_9',
        'infigo_snippet_style_10',
        'infigo_snippet_style_11',
        'infigo_snippet_style_12',
        'infigo_snippet_style_13',
        'infigo_snippet_style_14',
        'infigo_snippet_style_15',
        'infigo_snippet_style_17',
        'infigo_snippet_style_18',
    ],


    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}

