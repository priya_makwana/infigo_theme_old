{
    # Theme information
    'name' : 'Infigo Category Attribute',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show Main Attribute info for a Product in Category Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_shop'
    ],

    # Views
    'data': [
        'view/view.xml',
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
