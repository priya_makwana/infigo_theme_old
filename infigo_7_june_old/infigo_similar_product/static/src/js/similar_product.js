odoo.define('infigo_similar_product.similar_product', function(require) {
"use strict";

require('web.dom_ready');
var base = require("web_editor.base");
var ajax = require('web.ajax');
var utils = require('web.utils');
var core = require('web.core');
var _t = core._t;

	$(".similar_product").click(function()
	{
		$('.cus_theme_loader_layout').removeClass('hidden');		    
			var pid = $(this).attr('data-id');
			ajax.jsonRpc('/suggest_product', 'call', {'product_id':pid}).then(function(data) {
				$(".similar_product_popover").css("visibility","visible").addClass("zoom-fadein");
				$('.cus_theme_loader_layout').addClass('hidden');
				$('.similar_product_main-div').html(data);
				
			 	//Total similar product - products
			 	var similar_total = $('.similar_total_product').html();
			 	if(similar_total > 1){
					$('.similar_product_found').html(similar_total+ " Similar Products Found");
				}
				else if(similar_total == 1){
					$('.similar_product_found').html(similar_total+ " Similar Product Found");
				}
			})
			.fail(function(){alert()})
	});
	
	$(".common-close-btn").click(function(){
		$(".similar_product_popover").css("visibility","hidden");
	})
	
	$(document).on( 'keydown', function(e){
		if(e.keyCode === 27) {
			$(".similar_product_popover").css("visibility","hidden");
		}
	});
});

$(window).load(function(){
	/* Hide wishlist dropdown in customize option when it active false */
	var similar_active = $("body").find("div").hasClass("similar_shop_icon");
	if(similar_active){
		$("#similar_allow_option").css("display","inline-block");
	}else{
		$("#similar_allow_option").css("display","none");
	}
})
