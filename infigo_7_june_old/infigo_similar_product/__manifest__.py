{
    # Theme information
    'name' : 'Infigo Similar Product',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show Similar Products button in Category Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_shop'
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
