import odoo
from odoo import http
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.infigo_shop.controllers.main import infigoShop
 
 
class infigoSimilarProduct(http.Controller):
    @http.route(['/suggest_product'], type='json', auth="public", website=True)    
    def fetchProduct(self,product_id=None, **kwargs):
        pricelist_context = dict(request.env.context)
        if not pricelist_context.get('pricelist'):
            pricelist = request.website.get_current_pricelist()
            pricelist_context['pricelist'] = pricelist.id
        else:
            pricelist = request.env['product.pricelist'].browse(pricelist_context['pricelist'])

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)
        compute_currency=""
        if product_id :
            product_record = request.env['product.template'].search([['id','=',product_id]])    
            values = {}
            from_currency = request.env.user.company_id.currency_id
            to_currency = pricelist.currency_id
            compute_currency = lambda price: from_currency.compute(price, to_currency)
            
            if product_record.alternative_product_ids:
                values={
                    'product':product_record,
                    'compute_currency': compute_currency,
                    'pricelist': pricelist,
                }
            response = http.Response(template="infigo_similar_product.infigo_similar_product_record",qcontext=values)            
            return response.render()
            
