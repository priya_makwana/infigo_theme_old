odoo.define('quickview.gets_product', function(require) {	
	"use strict";	
		
	var base = require('web_editor.base');
	var ajax = require('web.ajax');	
	var utils = require('web.utils');
	var core = require('web.core');
	var _t = core._t;
	
	 // add website_sale & wishlist , compare and rating script for quick view
		
	function close_quickview(data)
	{                
		$("script[src='/infigo_wishlist/static/src/js/wishlist_script.js']").remove()
		$("script[src='/website_sale_comparison/static/src/js/website_sale_comparison.js']").remove()
		$("script[src='/website_sale/static/src/js/website_sale.js']").remove()
		$("script[src='/infigo_quick_view/static/src/js/quick_view_wishlist.js']").remove()
		$('.mask').css("display","none");
		$('.mask_cover').css("display","none");
		$('.mask_cover').empty(data);
	}
	var comparelist_product_ids = JSON.parse(utils.get_cookie('comparelist_product_ids') || '[]');
    function dispcompare()
    {
     	comparelist_product_ids = JSON.parse(utils.get_cookie('comparelist_product_ids') || '[]');
     	$('.o_product_circle').text(comparelist_product_ids.length)
     	$('.o_compare').attr('href', '/shop/compare/?products='+comparelist_product_ids.toString());
     	$('.cus_theme_loader_layout').addClass('hidden');
    }
	function get_quickview(pid)
	{
			
		pid=pid;
		ajax.jsonRpc('/productdata', 'call', {'product_id':pid}).then(function(data) {
			//console.log(data);
			
			$(".mask_cover").append(data)
			$(".mask").fadeIn();
			$('.cus_theme_loader_layout').addClass('hidden');
			$(".mask_cover").css("display","block");
	    	
			//Add Wishlist and Compare Script
			if ($('script[src="/infigo_wishlist/static/src/js/wishlist_script.js"]').length > 0) { 
				$("script[src='/infigo_wishlist/static/src/js/wishlist_script.js']").remove()
			}
			var compare_script=document.createElement('script');	    
     	    compare_script.type='text/javascript';	    
     	    compare_script.src="/website_sale_comparison/static/src/js/website_sale_comparison.js";	
			$("script[src='/website_sale_comparison/static/src/js/website_sale_comparison.js']").remove()
     	    $("head").append(compare_script);
			
			var sale_script=document.createElement('script');	    
     	    sale_script.type='text/javascript';	    
     	    sale_script.src="/website_sale/static/src/js/website_sale.js";	
			$("script[src='/website_sale/static/src/js/website_sale.js']").remove()
     	    $("head").append(sale_script);

			//Close Button Quickview
	    	$(".close_btn").click(function(){
	    		close_quickview(data)
	    	});
	    	
	    	//Esc to Close Quickview
	    	$(document).on( 'keydown', function(e){
	    		if(e.keyCode === 27){
	    			if(data){
	    				close_quickview(data)
	    			}
	    		}
	    	});
	    	// compare
			if($(".product_quick_view_class").find(".o_add_compare_dyn")) {
                dispcompare()
                
                $('.oe_website_sale .o_add_compare, .oe_website_sale .o_add_compare_dyn').click(function (e) {
                    $.getScript('/website_sale_comparison/static/src/js/website_sale_comparison.js', function (data, textStatus, jqxhr) {
                       //console.log(data)
                    	$('.cus_theme_loader_layout').removeClass('hidden');
                        var count = comparelist_product_ids.length;
                        if (count >= 4)
                            $(".compare_max_limit").css({"visibility": "visible", "z-index": "9999"});
                        setTimeout(function () {
                            $(".compare_max_limit").css("visibility", "hidden");
                        }, 2000);

                        dispcompare()
                    });
                })
            }
			//wishlist
			if($(".product_quick_view_class").find(".add2wish")) {
				var wish_script=document.createElement('script');	    
	     	    wish_script.type='text/javascript';	    
	     	    wish_script.src="/infigo_quick_view/static/src/js/quick_view_wishlist.js";	
				$("script[src='/infigo_quick_view/static/src/js/quick_view_wishlist.js']").remove()
	     	    $("head").append(wish_script);
			}
		});
	}
	return{
		get_quickview:get_quickview
	};
})
