{
    # Theme information
    'name' : 'Infigo Customize Category Attribute',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Allow Category Style Attribute for Customize shop page',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_category_attribute','infigo_shop_customize'
    ],

    # Views
    'data': [        
        'templates/template.xml',
       
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
