{
    # Theme information
    'name' : 'Infigo Customize Similar Products',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Similar Products Custom Style Attribute for Customize shop page',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_similar_product','infigo_shop_customize'
    ],

    # Views
    'data': [        
        'templates/template.xml',
       
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
