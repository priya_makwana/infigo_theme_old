$(window).load(function(){
	//Hide allow compare in customize  
	var compare_active = $("body").find("span").hasClass("compare_shop_icon");
	if(compare_active){
		$("#compare_allow_option").css("display","inline-block");
	}else{
		$("#compare_allow_option").css("display","none");
	}
	
	//Slider not call if compare product is lessthen 3
	var compare_count_span = $(".compare-count-span").html();
	if(compare_count_span > 3){
		$('.compare_slider_main > .owl-carousel').owlCarousel({
			loop:true,
			margin:10,
		    nav:true,
		    autoplay:false,
		    autoplayTimeout:3000,
		    autoplayHoverPause:true,
		    responsiveClass:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        700:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		  });
	}else{
		if ($(window).width() > 800) {
			$(".owl-carousel").css("display","block");
			$(".compare_main").addClass("non-carousel");
		}else{
			$(".compare_main").removeClass("non-carousel");
			$('.compare_slider_main > .owl-carousel').owlCarousel({
				loop:true,
				margin:10,
			    nav:true,
			    autoplay:false,
			    autoplayTimeout:3000,
			    autoplayHoverPause:true,
			    responsiveClass:true,
			    
			    responsive:{
			        0:{
			            items:1
			        },
			        700:{
			            items:2
			        },
			        1000:{
			            items:3
			        }
			    }
			  });
		}
		
	}
})
