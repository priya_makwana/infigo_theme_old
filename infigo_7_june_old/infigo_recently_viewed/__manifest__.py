{
    # Theme information
    'name' : 'Infigo Recently Viewed',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show Recently Viewed Products',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_wishlist','infigo_cart'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
