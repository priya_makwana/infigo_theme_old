$(window).load(function() {
	//zooming effect of video icon
	var animDuration = 500;
	$(".pvideo_link").addClass("i-zoom-animation");
    setTimeout(function(){
    $(".pvideo_link").removeClass("i-zoom-animation");
    }, animDuration
    );
});
$(document).ready(function() {  
	$(".pvideo_link").click(function(){ 
		$('.product_video_popup_main').css("display","block");
		$("iframe.popup_iframe_url").attr('src', $("iframe.popup_iframe_url").attr('src') + "?autoplay=1&amp;controls=0&amp;loop=1&amp;rel=0&amp;showinfo=0");
		$('body').css("overflow","hidden");
	})
	$(".popup-close").click(function(){ 
		$('.product_video_popup_main').css("display","none");
		$('body').css("overflow","visible");
	})
})

 
$(document).on( 'keydown', function(e){
	if(e.keyCode === 27) {
		$('.product_video_popup_main').css("display","none");
		$('body').css("overflow","visible");
	}
});	

$(window).load(function(){
	var suggest_count = $('.suggest_count').html();
	
		$('.ps_alt_sub_div > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:true,
		    autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:true,
		    responsiveClass:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        700:{
		            items:2
		        },
		        1000:{
		            items:2
		        }
		    }
		  });
	
	var acce_count = $('.acce_count').html();
	
		$('#ps_access_sub_div > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:true,
		    autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:true,
		    responsiveClass:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:2
		        }
		    }
		  });

		
		$('.breadcrumb li').each(function(){

		    var breadWidth = $(this).width();

		    if($(this).parent('li').hasClass('active') || $(this).parent('li').hasClass('first')){

		    

		    } else {

		        $(this).css('max-width', 200 + 'px');
		        $(this).css('text-overflow','ellipsis');

		        $(this).mouseover(function(){
		            $(this).css('max-width', breadWidth + 'px');
		            $(this).css('text-overflow','unset');
		        });

		        $(this).mouseout(function(){
		            $(this).css('max-width', 200 + 'px');
		            $(this).css('text-overflow','ellipsis');
		        });
		    }

		        
		});
	
})