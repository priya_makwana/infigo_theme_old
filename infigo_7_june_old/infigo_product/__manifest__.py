{
    # Theme information
    'name' : 'Infigo Product',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'View Complete Product Information',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_base','website_sale_stock'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'view/product_short_description.xml',
        'view/product_video.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
