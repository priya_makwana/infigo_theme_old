{
    # Theme information
    'name' : 'Infigo Shop Customize',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Customise your Category page with different Options ',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_shop'
    ],

    # Views
    'data': [
        'templates/assets.xml',
        'templates/template.xml',
        'templates/customize_template.xml',
       
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
