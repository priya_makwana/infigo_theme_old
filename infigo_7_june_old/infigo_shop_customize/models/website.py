from odoo import api, fields, models
 
class website(models.Model):
     
    _inherit = "website"
    
    text_align_style = fields.Selection([('left', 'Left'),('right', 'Right'),('center', 'Center')],default='left')
    box_border_style = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    allow_quick_view = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    show_label_style = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    images_to_show = fields.Selection([('4', '4'),('5', '5')],default='4')
    allow_attribute_category = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    allow_alternate_image = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    allow_similar_product = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    allow_wishlist = fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    allow_comparision =fields.Selection([('allow', 'Allow'),('deny', 'Deny')],default='allow')
    