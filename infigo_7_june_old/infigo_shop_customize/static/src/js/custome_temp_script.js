$(document).ready(function(){
	$(".custome_icon").click(function(){
		$(".popup_main_window").css("display","block").addClass("zoom-fadein");
	})
	
	$(".customize-option-close").click(function(){
		$(".popup_main_window").css("display","none");
		$(".popup_main_window_maxw").removeClass("zoom-fadein");
	})
	var url = location.pathname;
	$(".js_redirect").val(url);
});

/* press esc key to closed style popup*/
$(document).on( 'keydown', function(e){
	if(e.keyCode === 27) {
		$(".popup_main_window").css("display","none");
		$(".popup_main_window_maxw").removeClass("zoom-fadein");
	}
});

