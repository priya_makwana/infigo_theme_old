{
    # Theme information
    'name' : 'Infigo Employee Carousel',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Showcase your Employee using Infigo Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_carousel','website_hr','infigo_business_snippets',
    ],

    # Views
    'data': [
        'templates/assets.xml',
        'templates/employee_carousel_snippet.xml',
        'templates/employee_carousel_option.xml',
        'view/hr_employee.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
