{
    # Theme information
    'name' : 'Infigo Snippet Style 6',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_ecommerce_snippets'
    ],

    # Views
    'data': [
        'templates/style_9.xml',  
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}

