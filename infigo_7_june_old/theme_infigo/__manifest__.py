{
    # Theme information
    'name' : 'Theme Infigo',
    'category' : 'Theme/eCommerce',
    'version' : '11.0.2.1',
    'summary': 'Fully Responsive, Clean, Modern & Sectioned Odoo eCommerce Theme',
    'description': """.""",
    'license': 'OPL-1',

    # Dependencies
    'depends': [
            'website',  
            'infigo_403',
            'infigo_404',
            'infigo_account',
            'infigo_attribute_filter',
            'infigo_attribute_style_2',
            'infigo_brand',
            'infigo_layout1',
            'infigo_customize_theme',
		  	 'infigo_customize_color',
            'infigo_carousel_quick_view',
            'infigo_category',
            'infigo_category_attribute',
            'infigo_category_description',
            'infigo_compare_wishlist',
            'infigo_contactus',
            'infigo_business_carousel',
            'infigo_layout2',
            'infigo_event',
            'infigo_footer',
            'infigo_latest_blogs',
            'infigo_navigation',
            'infigo_notification',
            'infigo_ecommerce_carousel',
            'infigo_optional_product',
            'infigo_product_carousel_wishlist',
            'infigo_quick_view_compare',
            'infigo_quick_view_wishlist',
            'infigo_rating',
            'infigo_recently_viewed',
            'infigo_customize_shop_options',
            'infigo_shop_left_sidebar',
            'infigo_shop_list',
            'infigo_shop_load_products',
            'infigo_signin',
            'infigo_similar_product',
            'infigo_product_carousel_compare',
               

    ],


    # Views
    'data': [
  
    #     'data/company_data/company_data.xml',
    ],
   
    #Odoo Store Specific
    'live_test_url': 'https://goo.gl/WVL2NR',
    'images': [
        'static/description/main_poster.jpg',
        'static/description/main_screenshot.jpg',

    ],
    
    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com/',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',

    # Technical
    'installable': True,
    'application': True,

    'price': 199.00,
    'currency': 'EUR',
}
