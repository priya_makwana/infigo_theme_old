{
    # Theme information
    'name' : 'Infigo Optional Products',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Activate Slide Transition Effect of Optional Items in Product Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_product'
    ],

    # Views
    'data': [
        'templates/assets.xml',
        'templates/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
