{
    # Theme information
    'name' : 'Infigo Business CMS Blocks',
    'category' : 'Website',
    'version' : '1.0',
    'summary': '17 CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
	    'infigo_b_snippet_style_1',
        'infigo_b_snippet_style_2',
        'infigo_b_snippet_style_3',
        'infigo_b_snippet_style_4',
        'infigo_b_snippet_style_5',
        'infigo_b_snippet_style_7',
        'infigo_b_snippet_style_8',
        'infigo_b_snippet_style_9',
        'infigo_b_snippet_style_10',
        'infigo_b_snippet_style_11',
        'infigo_b_snippet_style_12',
        'infigo_b_snippet_style_13',
        'infigo_b_snippet_style_14',
        'infigo_b_snippet_style_15',
        'infigo_b_snippet_style_16',
        'infigo_b_snippet_style_17',
        'infigo_b_snippet_style_18',
	
    ],


    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}

