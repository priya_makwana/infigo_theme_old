{
    # Theme information
    'name' : 'Infigo Category Description',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Set Specific Description per each Category in Website Category Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_shop'
    ],

    # Views
    'data': [
       'view/product_category.xml',
       'templates/template.xml',
       'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
