{
    # Theme information
    'name' : 'Infigo Quick View Compare',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Compare from Quick View',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_compare','infigo_quick_view'
    ],

    # Views
    'data': [
        'templates/template.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
