$(window).load(function(){
		$('.emp1_main_div > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:false,
		    autoplay:true,
		    autoplayTimeout:2000,
		    responsiveClass:true,
		    autoplayHoverPause:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:2
		        }
		    }
		  });
		$('.customer_2_main_div > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:true,
		    autoplay:true,
		    autoplayTimeout:4000,
		    responsiveClass:true,
		    autoplayHoverPause:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		  });
		$('#svn_product_carousel_1 > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:true,
		    autoplay:true,
		    autoplayTimeout:2000,
		    responsiveClass:true,
		    autoplayHoverPause:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		  });
		$('#svn_product_carousel_2 > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:true,
		    autoplay:true,
		    autoplayTimeout:2000,
		    responsiveClass:true,
		    autoplayHoverPause:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		  });
		$('#thr_carousel_one > .owl-carousel').owlCarousel({
			loop:true,
		    margin:10,
		    nav:true,
		    autoplay:true,
		    autoplayTimeout:2000,
		    responsiveClass:true,
		    autoplayHoverPause:true,
		    
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		  });
})
