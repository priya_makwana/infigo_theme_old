{
    # Theme information
    'name' : 'Infigo Ecommerce Mega Store',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'A Unique MegaStore Layout with Clean and Creative Design to show off your Products in smartest ways',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_business_cms_blocks','infigo_ecommerce_cms_blocks'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
