import odoo
from odoo import http
from odoo.http import request

class infigoHomePage1(http.Controller):    
    
    @http.route(['/home/1'], type='http', auth="public", website=True)    
    def layout2(self, **kwargs):        
         return request.render("infigo_layout2.infigo_layout2_homepage")