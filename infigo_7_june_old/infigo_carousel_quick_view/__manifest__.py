{
    # Theme information
    'name' : 'Infigo Carousel Quick View',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Carousel from Quick View',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_quick_view','infigo_product_multi_carousel'
    ],

    # Views
    'data': [
        'template/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
