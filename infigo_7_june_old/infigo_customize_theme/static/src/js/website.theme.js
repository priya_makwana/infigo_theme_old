odoo.define('infigo_layout.infigo_themecolor', function (require) {
    'use strict';

    var ajax = require('web.ajax');
    var core = require('web.core');
    var session = require('web.session');
    var Widget = require('web.Widget');
    var QWeb = core.qweb;

    var infigo_themecolor = require('website.theme');

    infigo_themecolor.include({

        active_select_tags: function () {
            /* Data store using json*/
            var theme = []
            ajax.jsonRpc('/theme_color_store', 'call', {
                theme_color: theme
            }).then(function (data) {
                if(data){
                    $('#input_color').val(data);
                }
            });
            /* Apply color using json */

            $(".select_color").click(function () {
                var theme_color = $('#input_color').val();
                if (theme_color != '') {
                    console.log(theme_color);
                    if (theme_color.match(/rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/) || theme_color.match(/^\#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/)) {
                        ajax.jsonRpc('/theme_color_change', 'call', {
                            theme_color: theme_color
                        }).then(function (data) {
                        });
                    }
                    else {
                        $('.select_color').find("input").remove();
                        $('#theme_customize_modal').find('.cus_theme_loader').css('display', 'none');
                        alert("Not a Valid Color");
                        $('#theme_customize_modal').removeClass('in');
                        $('#theme_customize_modal').addClass('out');
                    }
                }
            });
        },
        update_style: function (enable, disable, reload) {
            if (this.$el.hasClass('loading')) {
                return;
            }
            this.$el.addClass('loading');

            if (this.$el.find('.cus_theme_loader').hasClass("hidden")) {
                this.$el.find('.cus_theme_loader').removeClass('hidden');
            }

            if (!reload && session.debug !== 'assets') {
                var self = this;
                return this._rpc({
                    route: '/website/theme_customize',
                    params: {
                        enable: enable,
                        disable: disable,
                        get_bundle: true,
                    },
                }).then(function (bundleHTML) {
                    var $links = $('link[href*=".assets_frontend"]');
                    var $newLinks = $(bundleHTML).filter('link');

                    var linksLoaded = $.Deferred();
                    var nbLoaded = 0;
                    $newLinks.on('load', function (e) {
                        if (++nbLoaded >= $newLinks.length) {
                            linksLoaded.resolve();
                        }
                    });
                    $newLinks.on('error', function (e) {
                        linksLoaded.reject();
                        window.location.hash = 'theme=true';
                        window.location.reload();
                    });

                    $links.last().after($newLinks);
                    return linksLoaded.then(function () {
                        $links.remove();
                        self.$el.removeClass('loading');
                        self.$el.find('.cus_theme_loader').addClass('hidden');
                    });
                });
            } else {
                var href = '/website/theme_customize_reload'+
                    '?href='+encodeURIComponent(window.location.href)+
                    '&enable='+encodeURIComponent(enable.join(','))+
                    '&disable='+encodeURIComponent(disable.join(','));
                window.location.href = href;
                return $.Deferred();
            }
        },
    });
});