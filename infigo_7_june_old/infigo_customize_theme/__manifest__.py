{
    # Theme information
    'name' : 'Infigo Customize Theme',
    'category' : 'Website',
    'version' : '1.0',
    'summary': '6 Preset Color Options to Cutomize Your Website',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_layout'
    ],

    #Views
    'data': [
        'templates/assets.xml',
        'templates/templates.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,

}
