from odoo import http
from odoo.http import request
import os


class themeColor(http.Controller):

    @http.route('/theme_color_change', type='json', auth="user", website=True)
    def color(self, theme_color=False, **kw):
        colorObj = request.env['ir.config_parameter']

        if theme_color:
            request.env['ir.config_parameter'].sudo().set_param("infigo_customize_theme.theme_color",
                                                                repr(theme_color))

            path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            path = path + "/static/src/less/custom_color.less"

            #...write file
            f = open(path, "w")
            f.write("@theme-color:"+theme_color+';')
            f.close()

    @http.route('/theme_color_store', type='json', auth="user", website=True)
    def color_store(self, theme_color=False, **kw):
        colorObj = request.env['ir.config_parameter']
        color_id = colorObj.sudo().get_param('infigo_customize_theme.theme_color', default='#7b92de')
        if color_id:
            color_id = color_id.replace("'", "")

            return color_id