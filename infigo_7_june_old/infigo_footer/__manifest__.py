{
    # Theme information
    'name' : 'Infigo Footer',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Infigo Custom Footer',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_layout'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
