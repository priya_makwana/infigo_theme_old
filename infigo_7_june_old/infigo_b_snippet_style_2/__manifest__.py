{
    # Theme information
    'name' : 'Infigo Business Snippet Style 2',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_business_snippets'
    ],

    # Views
    'data': [
        'templates/snippet_2.xml',  
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
