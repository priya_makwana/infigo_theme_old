{
    # Theme information
    'name' : 'Infigo Contact Us',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contact us Page',
    'description': """""",

    # Dependencies
    'depends': [
        'website_crm','infigo_layout'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
