odoo.define('product_static_carousel.snippets.animation', function (require)
{
'use strict';
var ajax = require('web.ajax');
var core = require('web.core');
var base = require('web_editor.base');
var utils = require('web.utils');
var animation = require('website.content.snippets.animation');
var no_of_product;
var qweb = core.qweb;

animation.registry.js_get_static_objects = animation.Class.extend
({
    selector : ".js_get_static_objects",
    start: function()
    {
      this.redrow();
    },
    stop: function(){
      this.clean();
    },

    redrow: function(debug)
    {
      this.clean(debug);
      this.build(debug);
    },

    clean:function(debug){
      this.$target.empty();
    },
    
    build: function(debug)
    {
    	
    	var self = this,
    	filter_id  = self.$target.data("filter_static_by_filter_id"),
        sale_label = self.$target.data("sale_label"),
        get_rating = self.$target.data("get_rating"),
    	template = self.$target.data("template");
    	$("#wait").css("display", "block");
        if(!template) template = 'infigo_product_multi_carousel.infigo_product_multi_carousel_static_carousel_snippet_heading';
        if(!sale_label)sale_label = 0;
        if(!get_rating)get_rating = 0;
        if(!filter_id)filter_id = false;
        var rpc_end_point = '/ecommerce_static_product_carousel_snippets/render';
        
        
        var comparelist_product_ids = JSON.parse(utils.get_cookie('comparelist_product_ids') || '[]');
        function dispcompare()
        {
        	comparelist_product_ids = JSON.parse(utils.get_cookie('comparelist_product_ids') || '[]');
        	var count = comparelist_product_ids.length;
        	$('.o_product_circle').text(comparelist_product_ids.length)
      	$('.o_compare').attr('href', '/shop/compare/?products='+comparelist_product_ids.toString());
      	$('.cus_theme_loader_layout').addClass('hidden');
      	if(count > 0){
      		$('.o_product_circle').text(comparelist_product_ids.length)
      	}else if(count == 0){
      		$('.o_product_circle').text("");
      	}
        }
        
        
        function wishcount(data)
    	  {
    		if(data.wishcount==0)
    		{
    			$(".wish_count").css("display","none")
    			$(".apply-wishlist").css("display","none")
    		}
    		else
    		{
    			$(".wish_count").css("display","inline-block")
    			$(".apply-wishlist").css("display","block")
    		}
    		$('.wish_count').html(data.wishcount)
    		$(".product").css("display","none");
    		for (var j = 0; j < data.wish_product.length; j++) 
    		{
    			var wish_pid = data.wish_product[j]
    			$(".add2wish[data-id='" + wish_pid + "']").css("display","none")
    			$(".add2wish_SC[data-id='" + wish_pid + "']").css("display","none")
    			$(".add2wish_MC[data-id='" + wish_pid + "']").css("display","none")
    			$(".in2wish[data-id='" + wish_pid + "']").css("display","inline-block")
    			$(".product[data-id='" + wish_pid + "']").css("display","block")
    		}
    	  }
        function addwish(pid)
        {
        	var url = ""
        	ajax.jsonRpc('/wishlist_products', 'call',{'product_id' : pid}).then(function(data) 
        	  		 {
        	  				if(data.user==false)
        	  				{
        	  						$('.login_modle').fadeIn();
        	  						$('.cus_theme_loader_layout').addClass('hidden');
        	  						$('.btnsubmit').click(function()
        	  						{
        	  									url =document.URL;
        	  									var userid=$('.email-textbox').val();
        	  									var pwd=$('.password-textbox').val();
        	  									ajax.jsonRpc('/login_web', 'call', {'userid' :userid,'passwd':pwd}).then(function(data) 
        	  									{
        	  											if(data.loginstatus==true)
        	  											{
        	  													$('.login_modle').css("display","none")
        	  													ajax.jsonRpc('/wishlist_products', 'call', {'product_id' : pid}).then(function(data) 
        	  													{
        	  															window.location.href = url;
        	  													})
        	  											}
        	  											else
        	  											{
        	  													$('.error').css("display","block")
        	  													$('.error').html("Invalid email or Password.")
        	  											}
        	  									})
        	  							})
        	  					}
        	  					else
        	  					{
        	  								wishcount(data)
        	  								$('.cus_theme_loader_layout').addClass('hidden');
        	  					}
        	  			})
        	  			.fail(function(e)
        	  			{
        	  					return;
        	  			})
        }
        function wish2compare()
        {
      	  
      	var compare_script=document.createElement('script');	    
     	    compare_script.type='text/javascript';	    
     	    compare_script.src="/website_sale_comparison/static/src/js/website_sale_comparison.js";	
  		$("script[src='/website_sale_comparison/static/src/js/website_sale_comparison.js']").remove()
     	    $("head").append(compare_script);
      	  if($("body").find(".o_add_compare ,.o_add_compare_dyn"))
  		  {
  				  dispcompare()
  				  
  			      $(".oe_website_sale .o_add_compare,.oe_website_sale .o_add_compare_dyn").click(function (e){
  			      	$.getScript('/website_sale_comparison/static/src/js/website_sale_comparison.js', function(data, textStatus,jqxhr ) {
  			      		$('.cus_theme_loader_layout').removeClass('hidden');
  			      		var count = comparelist_product_ids.length;
  			      		if(count >= 4)
  			      			$(".compare_max_limit").css("visibility","visible");
  			    			setTimeout(function(){
  			    				$(".compare_max_limit").css("visibility","hidden");
  			    			}, 2000);
  			      		dispcompare()
  			          });
  			      });
  		  }
      	if($("body").find(".add2wish"))
		  {
  		  ajax.jsonRpc('/wishlist_products', 'call', {}).then(function(data) 
  		  {
  				if(data.user!=false)
  				{
  					wishcount(data)
  				}
  				$('.cus_theme_loader_layout').addClass('hidden');
  		 });
		}
      	  
        }
        
        function addscript()
        {
      	  
      	    // add wish2compare
      	  	wish2compare()
      	  	if($("body").find(".add2wish"))
		   {
    	  	 var wish = self.$target.find('.add2wish')
    		 	wish.click(function() {
    			 $('.cus_theme_loader_layout').removeClass('hidden');
    			 var pid = $(this).attr('data-id');
    			 addwish(pid)
    		  });
		   }
      	  	
      	   
  			if($("body").find(".quick-view-a"))
  			{
  			var qview = self.$target.find('.quick-view-a')
  	  		qview.click(function() {
  			$('.cus_theme_loader_layout').removeClass('hidden');
  	   		var pid = $(this).attr('data-id');
  	   		ajax.jsonRpc('/productdata', 'call', {'product_id':pid}).then(function(data) 
  	   		{	
  	   				$(".mask_cover").append(data)
  	   				$(".mask").fadeIn();
  	   				$('.cus_theme_loader_layout').addClass('hidden');
  	   				$(".mask_cover").css("display","block");
  	   				    	
  	   				var sale_script=document.createElement('script');	    
  	   		     	sale_script.type='text/javascript';	    
  	   		     	sale_script.src="/website_sale/static/src/js/website_sale.js";	
  	   		     	$("script[src='/website_sale/static/src/js/website_sale.js']").remove()
  	   		     	$("head").append(sale_script);
  	   		     	
  	   		     	if($( ".product_quick_view_class" ).find( "q_rating-block" ))
  	   		     	{	  
  	   		     		$("script[src='/infigo_rating/static/src/js/rating_script.js']").remove();
  	   		     		var rating_script=document.createElement('script');
  	   		     		rating_script.type='text/javascript';
  	   		     		rating_script.src="/infigo_rating/static/src/js/rating_script.js";
  	   		     		$("head").append(rating_script);
  	   		     	}
  	   		     	
  	   		     	// add wish2compare
  	   		     	wish2compare()
  	   		     	if($( ".product_quick_view_class" ).find(".add2wish"))
	   		     	 {
	   		     	 $(".product_quick_view_class .add2wish").click(function() {
	   		     		 $('.cus_theme_loader_layout').removeClass('hidden');
	   		     		 var pid = $(this).attr('data-id');
	   		     		 addwish(pid)
	   		     	 });
	   		     	 }
  	   		     	
  	   		     	$(".close_btn").click(function()
  	   		   		{
  	   		   		   		$('.mask_cover').empty(data);
  	   		   		});
  	   		   		$(document).on('keydown', function(e)
  	   		   		{
  	   		   		 		if(e.keyCode === 27) 
  	   		   		  		{
  	   		   		   			if(data)
  	   		   		  			{
  	   		   		   				$('.mask_cover').empty(data);
  	   		   		   			}
  	   		   		   		}
  	   		   		});
  	   			});
  	  		});
  		}
        }
        function addoption()
        {
      	  
      	  if(get_rating==0)
  		  {   			 
  			 	self.$target.find('.product_carousel_rating').css("display","none")
  		  }
  		  else
  		  {
  			  	self.$target.find('.product_carousel_rating').css("display","block")
  			  	$("script[src='/infigo_rating/static/src/js/rating_script.js']").remove()	
  			  	if($( "div" ).hasClass( "product_carousel_rating" ))
  			  	{	 
  			  		var rating_script=document.createElement('script');	    
  			  		rating_script.type='text/javascript';	    
  			  		rating_script.src="/infigo_rating/static/src/js/rating_script.js";
  			  		$("head").append(rating_script);
  			  	}
  		  }
  	  	    	// for label
  				if(sale_label==0)
  					self.$target.find('.label-block').css("display","none")
  				else
  					self.$target.find('.label-block').css("display","block")
        }
        
        ajax.jsonRpc(rpc_end_point, 'call', {'template': template,'filter_id': filter_id,}).then(function(objects) 
        {
        	$(objects).appendTo(self.$target);
        	self.$target.find(".filter_static_title:first").addClass("active_tab")
        	var temp_id=self.$target.find(".filter_static_title:first").attr("data-id")
        	if(!temp_id)temp_id = false;
        	ajax.jsonRpc('/static_product_data', 'call', {'template': template,'temp_filter_id': temp_id,}).then(function(data) 
    		{	
        		var cont_tab=(self.$target).find(".contenttab")
        		$(cont_tab).html(data);
        		addscript()
        		$('.cus_theme_loader_layout').addClass('hidden');
        		self.$target.find("div[class='fun_slide_class']").removeClass("fun_slide_class").addClass('non')
        		addoption()
     	  })
     	  $(".filter_static_title").click(function()
    	  {
				$('.cus_theme_loader_layout').removeClass('hidden');
    			var curr_tag=$(this);
    			var curr_tag_id=curr_tag.attr("data-id");
    			$(".filter_static_title").removeClass("active_tab")
    			curr_tag.addClass("active_tab")
    			ajax.jsonRpc('/static_product_data', 'call', {'template': template,'temp_filter_id': curr_tag_id}).then(function(data) 
    			{
    				
    				$('.cus_theme_loader_layout').addClass('hidden');
    				var cont_tab=(self.$target).find(".contenttab")
    				$(cont_tab).html(data);
    				addscript()
    				addoption()
    				self.$target.find("div[class='fun_slide_class']").removeClass("fun_slide_class").addClass('non');
        			$(".non").addClass("zoom-animation");
    				setTimeout(function(){
    					$(".non").removeClass("zoom-animation");
    				},500);

    		  })
    	})	  	    
    }).then(function(){
  	  self.loading(debug);
    }).fail(function(e) {
      
    });
    },
    loading: function(debug){
    	//function to hook things up after build    	
    }
    
	});
});
