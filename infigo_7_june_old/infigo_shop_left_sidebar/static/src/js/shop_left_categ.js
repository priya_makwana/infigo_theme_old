$(document).ready(function(){
	//var find_var = $('body').find('.oe_shop_left');
	if($('div').hasClass('oe_shop_left'))
		{
			$("div").find(".main").addClass("main_left");
			$("div").find("#products_grid").addClass("main_left_grid");
			$("div").find("#products_grid_before").addClass("main_left_grid_before");
			$("div").find(".right-cnt-maxW").addClass("right-cnt-maxW_left");
			$("div").find(".menu_view_filter_div").addClass("left_side_bar_view_filter");
			//$("div").find(".products-grid-main").removeClass("main_left_grid");
			$(".menu-filter").unbind("click"); 
			$('.menu-filter').css("display","none");
			$("div").find(".right-cnt-maxW").removeClass("right-cnt-maxW");
			
			$("div").find(".sub_breadcrumb").addClass("maxW_div");
			$("div").find("#shop-container").addClass("maxW_div");
			if ($(window).width() > 900) {
				$('.products_category_ul').removeClass('mCustomScrollbar').removeAttr('data-mcs-theme');
			}
			$(".filter_right_ul").removeClass("mCustomScrollbar");
			$(".shop-breadcrumb").css({'max-width':'1400px','width':'90%'});
			$('#image_show').css("display","none");
			
			//Push toggle for filter option
			if($(window).width() > 700){
				$('.menu-filter').click(function(){
					$("#products_grid_before").css({width:"100%","transition":"0.5s","padding":"0"});
					$("#wrapwrap").css({"margin-left":"0px","transition":"0.5s"});
					
					var dyn_categ_height = $("#products_grid_before").find('.js_attributes').height();
					var dyn_height = $("#products_grid_before").height();
					var categ_height = $("#products_grid_before").find('#o_shop_collapse_category').height();
					
					$("#products_grid_before").find('.attribute-filter-div').closest('li').css({
						
					})
					
					if($("form.js_attributes input:checked").prop('checked') == true){
						$(this).find('.attribute-filter-div .clear-all-variant').css({
							display:'inline-block',
							'font-size': '7px'
						});
					}else{
						$('#pull-out').css('display','none')
					}
					
					$('body').css("overflow-x","hidden");
					$(".transparent").css("display","block");
					
					if($(window).width() < 770) {
						$("#products_grid_before").removeClass('hidden-xs');
					}
				});
				
			}else{
				$("#products_grid_before").removeClass('hidden-xs');
				$('.menu-filter').click(function(){
					$("#products_grid_before").css({"width":"300px","transition":"all 0.5s ease 0s","padding-left":"2%"});
					$("#wrapwrap").css({"margin-left":"300px","transition":"0.5s"});
					$('body').css("overflow-x","hidden");
					$(".transparent").css("display","block");
					
					$("#o_shop_collapse_category,.sub-li-main").mCustomScrollbar('destroy');
					$(".filter_right_ul").removeClass('mCustomScrollbar');
					$("#products_grid_before").find('#mCSB_2,#mCSB_3,#mCSB_2_container,#mCSB_3_container').removeAttr('style')
					
				});
			}
	}
		
	//screen 900 to hide filter

	if ($(window).width() < 900) {
		$('.menu-filter').css("display","block");
		//$('#products_grid_before').css("display","none");
		$('.main_left_grid').css("cssText", "width: 100% !important;");
		$("#products_grid_before").removeClass("main_left_grid_before");
		$("div").find(".menu_view_filter_div").removeClass("left_side_bar_view_filter");
	}
	if( $(".css_editable_display").css('display') == 'block') {
		$('.view-as').css('display','none');
	}
})

$(window).load(function(){
	var filter_label = $('.main_left').parent().find('.view-as-div').css("margin-left","0px");
	var filter_label = $('.main_left').parent().find('label.view-label').addClass('filter-label-alignment');
})


