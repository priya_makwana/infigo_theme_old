{
    # Theme information
    'name' : 'Infigo Rating',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show Product Rating ',
    'description': """""",

    # Dependencies
    'depends': [
        'website_rating','infigo_wishlist','infigo_product_multi_carousel','infigo_quick_view','infigo_compare',
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
