from odoo import http, _
from odoo.http import request
from odoo.addons.infigo_shop.controllers.main import infigoShop
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.infigo_compare.controllers.main import infigoproductcomparison
from odoo.addons.infigo_wishlist.controller.main import WebsiteSaleWishlist        

class infigoRating(infigoShop):
    
    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        response = super(infigoRating, self).shop(page=page, category=category, search=search, **post)
        Rating = request.env['rating.rating']
        products = response.qcontext['products']
        rating_templates = {}
        for product in products :
            ratings = Rating.search([('message_id', 'in', product.website_message_ids.ids)])#             rating_message_values = dict([(record.message_id.id, record.rating) for record in ratings])
            rating_product = product.rating_get_stats([('website_published', '=', True)])
            rating_templates[product.id] = rating_product
            response.qcontext['rating_product'] = rating_templates
        return response 
    
class infigoProductRating(WebsiteSale):
    
    @http.route(['/shop/product/<model("product.template"):product>'], type='http', auth="public", website=True)
    def product(self, product, category='', search='', **kwargs): 
        response = super(infigoProductRating, self).product(product=product, category=category, search=search, **kwargs)
        Rating = request.env['rating.rating']
        rating_templates = {}
        ratings = Rating.search([('message_id', 'in', product.website_message_ids.ids)])#             rating_message_values = dict([(record.message_id.id, record.rating) for record in ratings])
        rating_product = product.rating_get_stats([('website_published', '=', True)])
        rating_templates[product.id] = rating_product
        response.qcontext['rating_product'] = rating_templates
        return response
    
class infigoCompareRating(infigoproductcomparison):
     
    @http.route('/shop/compare/', type='http', auth="public", website=True)
    def product_compare(self, **post): 
        response = super(infigoCompareRating, self).product_compare(**post)
        Rating = request.env['rating.rating']
        rating_templates = {}
        products = response.qcontext.get('products') or None
        if products:
            for product in products :
                ratings = Rating.search([('message_id', 'in', product.product_tmpl_id.website_message_ids.ids)])#             rating_message_values = dict([(record.message_id.id, record.rating) for record in ratings])
                rating_product = product.product_tmpl_id.rating_get_stats([('website_published', '=', True)])
                rating_templates[product.id] = rating_product
        response.qcontext['rating_product'] = rating_templates
        return response

class WebsiteSaleWishlistRating(WebsiteSaleWishlist):
     
    @http.route(['/shop/wishlist'], type='http', auth="public", website=True)
    def get_wishlist(self,page=0, category=None, search='', **kwargs):
        response = super(WebsiteSaleWishlistRating, self).get_wishlist(page=page, category=category, search=search, **kwargs)
        Rating = request.env['rating.rating']
        objects = response.qcontext.get('object',False) 
        rating_templates = {}
        if objects : 
            for product in objects:
                ratings = Rating.search([('message_id', 'in', product.product_id.website_message_ids.ids)])#             rating_message_values = dict([(record.message_id.id, record.rating) for record in ratings])
                rating_product = product.product_id.rating_get_stats([('website_published', '=', True)])
                rating_templates[product.product_id.id] = rating_product
                response.qcontext['rating_product'] = rating_templates
        return response 
        