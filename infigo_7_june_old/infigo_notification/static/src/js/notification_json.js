odoo.define('infigo_notification.get_filter_product', function(require) {	
"use strict";	

require('web.dom_ready');
var base = require("web_editor.base");
var ajax = require('web.ajax');
var utils = require('web.utils');
var core = require('web.core');
var _t = core._t;
	 

	var notification =setInterval(function(){ 
		$(".notification-div  i.fa-bell").addClass("notification_shack");
	}, 3000);
	setInterval(function(){ 
		$(".notification-div  i.fa-bell").removeClass("notification_shack");
	},2000);
	$(".notification-div").click(function(){ 
		$(".notification_popover").css({"display":"block","transition ":"1s ease-in-out"});
		clearInterval(notification);
		$(".notification-div  i.fa-bell").removeClass("notification_shack");
		$(".close-notification").css("display","block");
		$('.cus_theme_loader_layout').removeClass('hidden');
		ajax.jsonRpc('/filterproduct', 'call').then(function(data) {
			$(".notification_popover").html(data);
			$('.cus_theme_loader_layout').addClass('hidden');
			$(".deal_add_to_cart .a-submit").click(function(event){
				event.stopPropagation();
				var id = $(this).attr("data-id")
				var product_added = $(this).next("span.deal-product-added");
				var current_add_cart_btn = $(this);
				$('.cus_theme_loader_layout').removeClass('hidden');
				ajax.jsonRpc('/dealaddcart', 'call' , {'id':id}).then(function(values) {
					$('.cus_theme_loader_layout').addClass('hidden');
					var total_amount=values.cart_total_amount;
					$(".my_cart_quantity").replaceWith('<span class="my_cart_quantity">'+values.cart_qty_total+'</span>');
					$(".cart-header-total").replaceWith('<span class="cart-header-total">'+total_amount.toFixed(2)+' '+ values.currency_symbol +'</span>');
					current_add_cart_btn.addClass("flash");
					product_added.addClass("notification_shack");
					product_added.addClass("added_block");
					setInterval(function(){ 
						product_added.removeClass("notification_shack");
						current_add_cart_btn.removeClass("flash");
					},500);
				})
			});
		})
	})
	$(".close-notification").click(function(event){
		event.stopPropagation();
		$(".notification_popover").css("display","none");
		$(".close-notification").css("display","none");
	})
	$(".notification-div").addClass("header-static-cnt");	
})
