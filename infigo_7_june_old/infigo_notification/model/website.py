from odoo import api, fields, models
 
class website(models.Model):
     
    _inherit = "website"
    
    notification = fields.Many2one('notification',string="Notification")
    wishlist_offer = fields.Many2one('notification',string="Wishlist Offer")