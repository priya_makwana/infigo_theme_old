from odoo import fields,models
from docutils.nodes import field

class res_config_settings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    notification = fields.Many2one('notification',string="Notification",related='website_id.notification')
    wishlist_offer = fields.Many2one('notification',string="Wishlist offer",related='website_id.wishlist_offer')