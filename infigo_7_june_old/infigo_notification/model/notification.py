from odoo import models, fields

class notification(models.Model):
    _name="notification"
    
    name=fields.Char("Notification Name",required=True)
    notification_ids = fields.Many2many('product.product', 'id',required=True)
   
