import odoo
from odoo import http
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.infigo_shop.controllers.main import infigoShop
from odoo.addons.infigo_wishlist.controller.main import WebsiteSaleWishlist

class infigoNotification(http.Controller):
    @http.route(['/filterproduct'], type='json', auth="public", website=True)    
    def fetchFilterProduct(self, **kwargs):
        notification_filter_id = request.website.notification.id
        notification_record = request.env['notification'].search([('id','=',int(notification_filter_id))])
        compute_currency, pricelist_context, pricelist = infigoShop()._get_compute_currency_and_context()
        order_arr = []
        order=request.website.sale_get_order()
        if order:
            for line in order.website_order_line:
                order_arr.append(line.product_id.id) 
        values={
            'notification':notification_record,
            'compute_currency': compute_currency,
            'pricelist': pricelist,
            'order_arr'   :order_arr,
        }
        response = http.Response(template="infigo_notification.infigo_notification",qcontext=values)            
        return response.render()
    
    @http.route(['/dealaddcart'], type='json', auth="public", methods=['POST'], website=True, csrf=False)    
    def addToCart(self,id=None, **kwargs):
        if id:
            request.website.sale_get_order(force_create=1)._cart_update(
                product_id=int(id),
                    add_qty=float(1),
                    set_qty=float(0),
                )
          
        order=request.website.sale_get_order()
        values={
            'cart_qty_total'   : order.cart_quantity,
            'cart_total_amount'   : order.amount_total,
            'currency_symbol': order.currency_id.symbol
        }
        return values
    
class infigoNotificationwishlist(WebsiteSaleWishlist):
    @http.route(['/shop/wishlist'], type='http', auth="public", website=True)
    def get_wishlist(self,page=0, category=None, search='', **kwargs):
        response = super(infigoNotificationwishlist, self).get_wishlist(page=page, category=category, search=search, **kwargs)
        wishlist_filter_id = request.website.wishlist_offer.id
        wishlist_record = request.env['notification'].search([('id','=',int(wishlist_filter_id))])
        response.qcontext['notification'] = wishlist_record
        return response
     
        
  
    
    
    
    
    
