{
    # Theme information
    'name' : 'Infigo Notification',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show your Latest Products in Header and Wishlist using Notification',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_wishlist',
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'view/notification_filter_view.xml',
        'security/ir.model.access.csv',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
