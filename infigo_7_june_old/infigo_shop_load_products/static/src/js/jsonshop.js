odoo.define('shop', function(require) {
	"use strict";
	
	require('web.dom_ready');
	require('website_sale.website_sale');
	var base = require('web_editor.base');
	var ajax = require('web.ajax');
	var utils = require('web.utils');
	var core = require('web.core');
	var _t = core._t;
	
	var total_page=$(".oe_total_page").val();
	var curr_page=$(".oe_page").val();
	if(total_page == curr_page)
	{
		$(".p_class_load_more_post").css("display","none")
	}
		
	$(".p_class_load_more_post").click(function() {
		var domain=$(".oe_attr").val()
		var attrib=$(".oe_attr_set").val()
		$(".oe_page").val(parseInt($(".oe_page").val(), 10)+1)
		var curr_page=$(".oe_page").val();
		$(".oe_att_set").val("")
		$(".js_attr").each(function(){
			if ($(".oe_att_set").val())
				$(".oe_att_set").val($(".oe_att_set").val()+","+$(this).attr('data-id'))
			else
				$(".oe_att_set").val($(this).attr('data-id'))
		})
		
		if(attrib == "")
			attrib = false
		var offset = $("a[data-offset='offset']").length
		
		var category=$(".oe_category").val()
		
		$('.cus_theme_loader_layout').removeClass('hidden');
		var order = $(".oe_order").val();
		ajax.jsonRpc('/load_shop', 'call', {'categ':category,'offset':offset,'order':order,'domain':domain,'attrib':attrib,'attr':$(".oe_att_set").val()}).then(function(data) {
			
			var attribute = $(data).find("#products_grid_before").find('.new_product').html();
			$(".filter_right_ul").append(attribute)
			var products = $(data).find("td").html();
			$("td").append(products)
			$('.cus_theme_loader_layout').addClass('hidden');
			var load_count = $(".remain_count").html()-$(".oe_remain_count").val()
			if(load_count > 1)
				$(".remain_count").html(load_count);
			else
				$(".remain_count_span").html('<span class="remain_count_span">( <span class="remain_count">' + load_count +'</span> Product ) </span>');
			
			if(total_page == curr_page)
			{
				$(".p_class_load_more_post").css("display","none")
			}
			
			if ($('script[src="/infigo_wishlist/static/src/js/wishlist_script.js"]').length > 0) { 
				$("script[src='/infigo_wishlist/static/src/js/wishlist_script.js']").remove()
			}
			if ($('script[src="/infigo_quick_view/static/src/js/quickview_script.js"]').length > 0) { 
				$("script[src='/infigo_quick_view/static/src/js/quickview_script.js']").remove()
			}
			if ($('script[src="/infigo_similar_product/static/src/js/similar_product.js"]').length > 0) { 
				$("script[src='/infigo_similar_product/static/src/js/similar_product.js']").remove()
			}
			if ($('script[src="/website_sale_comparison/static/src/js/website_sale_comparison.js"]').length > 0) { 
				$("script[src='/website_sale_comparison/static/src/js/website_sale_comparison.js']").remove()
			}
			if ($('script[src="/infigo_compare/static/src/js/compare.js"]').length > 0) { 
				$("script[src='/infigo_compare/static/src/js/compare.js']").remove()
			}
			if ($('script[src="/infigo_rating/static/src/js/rating_script.js"]').length > 0) { 
				$("script[src='/infigo_rating/static/src/js/rating_script.js']").remove()
			}
			if ($('script[src="/website_sale_options/static/src/js/website_sale.js').length > 0) { 
				$("script[src='/website_sale_options/static/src/js/website_sale.js").remove()
			}
			if ($('script[src="/infigo_shop_load_products/static/src/js/website_sale_json.js').length > 0) { 
				$("script[src='/infigo_shop_load_products/static/src/js/website_sale_json.js").remove()
			}
			
			add_load_more_script();
		})
	})
	
	function add_load_more_script(){
		
	    
		var q_script=document.createElement('script');
	    var w_script=document.createElement('script');
	    var base_script=document.createElement('script');
	    var similar_script=document.createElement('script');
	    var compare_script=document.createElement('script');
	    var infigo_compare_script=document.createElement('script');
	    var rating_script=document.createElement('script');
	    
	    w_script.type='text/javascript';
	    w_script.src="/infigo_wishlist/static/src/js/wishlist_script.js";
	    
	    q_script.type='text/javascript';
	    q_script.src="/infigo_quick_view/static/src/js/quickview_script.js";
	    
	    base_script.type='text/javascript';
	    var option_cart = $('.oe_script').val();
	    base_script.src = option_cart;
	    	    
	    similar_script.type='text/javascript';
	    similar_script.src="/infigo_similar_product/static/src/js/similar_product.js";

	    compare_script.type='text/javascript';
	    compare_script.src="/website_sale_comparison/static/src/js/website_sale_comparison.js";
	    
	    infigo_compare_script.type='text/javascript';
	    infigo_compare_script.src="/infigo_compare/static/src/js/compare.js";
	   
	    
	    rating_script.type='text/javascript';
	    rating_script.src="/infigo_rating/static/src/js/rating_script.js";
	    
	    $("head").append(q_script);
	    $("head").append(w_script);
	    $("head").append(base_script);
	    $("head").append(similar_script);
	    $("head").append(compare_script);
	    $("head").append(infigo_compare_script);
	    $("head").append(rating_script);
	}
})	
	
	
