# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import json
import logging
from werkzeug.exceptions import Forbidden
import math
from odoo import http, tools, _
from odoo.http import request
from odoo.addons.base.ir.ir_qweb.fields import nl2br
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.addons.infigo_shop.controllers.main import infigoShop

PPG = 8  # Products Per Page
PPR = 4   # Products Per Row

class Jsonshop(http.Controller):
    
    @http.route(['/load_shop'], type='json', auth="public", website=True)
    def shopjson(self, page=0, categ=None,attrib=None,order=None,attr=None,domain=[],offset=4,search='',**post):
        compute_currency, pricelist_context, pricelist = infigoShop()._get_compute_currency_and_context()
        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)
        Product = request.env['product.template']
        domain = eval(domain)
        
        att = []
        if attr:
            attr_val = attr.split(",")
            for aval in  attr_val:
                att.append(aval)
        
       
        category = request.env['product.public.category'].search([("id","=",categ)])
        keep = QueryURL('/shop', category=category and int(category), search=search, attrib="",order=order)
        
        if not order:
            order= 'website_published desc,website_sequence desc , id desc'
             
        product_count = Product.search_count(domain)     
        products = Product.search(domain, limit=8, offset=offset, order=order)
        ProductAttribute = request.env['product.attribute']
        attributes = None
        if products:
            attributes = ProductAttribute.search([('attribute_line_ids.product_tmpl_id', 'in', products.ids),('id','not in',att)])
        
        if not attrib:
            attrib = {}
        
          
        Rating = request.env['rating.rating']
        
        rating_templates = {}
        for product in products :
            ratings = Rating.search([('message_id', 'in', product.website_message_ids.ids)])
            rating_message_values = dict([(record.message_id.id, record.rating) for record in ratings])
            rating_product = product.rating_get_stats([('website_published', '=', True)])
            rating_templates[product.id] = rating_product
            
        values = {
            'rows': PPR,
            'products': products,
            'remain_count':len(products),
            'search_count': product_count,  # common for all searchbox
            'keep': keep,
            'compute_currency': compute_currency,
            'attrib_set':attrib,
            'attributes': attributes,
            'rating_product':rating_templates,
            'category': category,
        }
        
        response = http.Response(template="infigo_shop_load_products.jsonproducts", qcontext=values)
        return response.render()


    
