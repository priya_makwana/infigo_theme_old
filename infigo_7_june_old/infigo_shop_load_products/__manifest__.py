{
    # Theme information
    'name' : 'Infigo Shop Load Products',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'As an alternative of Pager, activate Ajax Based Load more Items button in Category page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_shop',
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'view/view.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
