{
    # Theme information
    'name' : 'Infigo Snippet Style 1',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_ecommerce_snippets'
    ],

    # Views
    'data': [
        'template/style_2.xml',  
        'template/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
