{
    # Theme information
    'name' : 'Infigo Blog',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Easy & User Friendly Blogging Platform for your Online Store',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_layout'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'data/blog_demo.xml',
        'data/website_blog_demo.xml',
      
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
