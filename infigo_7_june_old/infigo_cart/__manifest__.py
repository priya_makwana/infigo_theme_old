{
    # Theme information
    'name' : 'Infigo Cart',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contains Shopping Cart Layout changes',
    'description': """""",

    # Dependencies
    'depends': [
        'website_sale_delivery','website_sale_options','infigo_shop'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
