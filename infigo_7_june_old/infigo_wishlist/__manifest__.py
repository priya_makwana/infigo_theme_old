{
    # Theme information
    'name' : 'Infigo Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'View List of Products added to Wishlist',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_shop','infigo_product','website_sale_wishlist'
    ],

    # Views
    'data': [
        'security/ir.model.access.csv',
        'template/theme_template.xml',
        'view/website.xml',
        'view/wishlist_view.xml',
        #'view/wishlist_line_view.xml',
        'template/wishlist_template.xml',
        'template/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
