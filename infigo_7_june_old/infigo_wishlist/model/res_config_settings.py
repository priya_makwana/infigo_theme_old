from odoo import fields,models
from docutils.nodes import field

class res_config_settings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    wishlist_login_model_image=fields.Binary("Wishlist Login Model Image",related='website_id.wishlist_login_model_image')
    