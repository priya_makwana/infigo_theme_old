from odoo import models, fields, api
 
 
class wishlist_wishlist(models.Model):
    _name = 'wishlist.wishlist'
    _rec_name="user_id"   

    user_id=fields.Many2one('res.users','Customer')
    wishlist_ids= fields.One2many('wishlist.wishlist.line', 'wishlist_id', 'Wishlist Lines' )
