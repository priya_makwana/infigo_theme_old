{
    # Theme information
    'name' : 'Infigo Compare Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Products into Wishlist from Compare Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_compare','infigo_wishlist'
    ],

    # Views
    'data': [
        'template/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
