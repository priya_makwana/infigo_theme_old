{
    # Theme information
    'name' : 'infigo Price Filter',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Filter Products by Price Range',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_shop'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
