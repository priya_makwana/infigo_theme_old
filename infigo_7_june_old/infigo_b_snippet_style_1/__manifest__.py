{
    # Theme information
    'name' : 'Infigo Business Snippet Style 1',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_business_snippets'
    ],

    # Views
    'data': [
        'templates/snippet_1.xml',  
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
