{
    # Theme information
    'name' : 'Infigo Customize Shop Options',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contains 5 modules for Customize Category Page feature',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_customize_category_attribute',
       'infigo_customize_compare',
       'infigo_customize_quick_view',
       'infigo_customize_similar_products',
       'infigo_customize_wishlist',
    ],


    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}

