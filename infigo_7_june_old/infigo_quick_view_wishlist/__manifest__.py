{
    # Theme information
    'name' : 'Infigo Quick View Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Wishlist from Quick View',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_wishlist','infigo_quick_view'
    ],

    # Views
    'data': [
        'templates/template.xml'
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
