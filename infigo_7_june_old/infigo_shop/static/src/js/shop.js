$(document).ready(function(){ 
	
	/*view filter click */
	$(".apply-filter").click(function(){
		$(".main-header-maxW").removeClass("transparent");
		$(".filter-main-div").css("display","block").addClass("zoom-fadein");
	});
	
	var load_count = $(".oe_count").val()-$(".oe_remain_count").val()
	if(load_count > 1)
		$(".remain_count").html(load_count)
	else
		$(".remain_count_span").html('<span class="remain_count_span">( <span class="remain_count">' + load_count +'</span> Product ) </span>');
	
	//Push toggle for filter option
	if($(window).width() > 700){
		$('.menu-filter').click(function(){
			$("#products_grid_before").css({width:"100%","transition":"0.5s","padding":"0"});
			$("#wrapwrap").css({"margin-left":"0px","transition":"0.5s"});
			
			var dyn_categ_height = $("#products_grid_before").find('.js_attributes').height();
			var dyn_height = $("#products_grid_before").height();
			var categ_height = $("#products_grid_before").find('#o_shop_collapse_category').height();
			
			$("#products_grid_before").find('.attribute-filter-div').closest('li').css({
				
			})
			
			if($("form.js_attributes input:checked").prop('checked') == true){
				$(this).find('.attribute-filter-div .clear-all-variant').css({
					display:'inline-block',
					'font-size': '7px'
				});
			}else{
				$('#pull-out').css('display','none')
			}
			/****************/
			
			$('body').css("overflow-x","hidden");
			$(".transparent").css("display","block");
			
			if($(window).width() < 770) {
				$("#products_grid_before").removeClass('hidden-xs');
			}
			if($(window).width() < 600){
				
			}
		});
		
	}else{
		$("#products_grid_before").removeClass('hidden-xs');
		$('.menu-filter').click(function(){
			$("#products_grid_before").css({"width":"300px","transition":"all 0.5s ease 0s","padding-left":"2%"});
			$("#wrapwrap").css({"margin-left":"300px","transition":"0.5s"});
			$('body').css("overflow-x","hidden");
			$(".transparent").css("display","block");
			$("#o_shop_collapse_category,.sub-li-main").mCustomScrollbar('destroy');
			$(".filter_right_ul").removeClass('mCustomScrollbar');
			$(".filter_block").mCustomScrollbar('destroy');
			$("#products_grid_before").find('#mCSB_2,#mCSB_3,#mCSB_2_container,#mCSB_3_container').removeAttr('style')
		});
	}
	
	//Show clear all link when any attribute is selected on load
	$("form.js_attributes input:checked").each(function(){ 
		var self=$(this)
		//For Color
		var curr_parent=self.parent().closest("li").find(".attribute-filter-div"); 
		var clear_link = curr_parent.find("a.clear-all-variant");
		//For Checkbox
		var parent_for_checkbox = self.closest("ul").closest("li").find(".attribute-filter-div").find("a.clear-all-variant");
		//clear_link.css({display:"inline-block",float:'right'});
		parent_for_checkbox.css({
			display:'inline-block',
			'font-size':'9px',
			'margin-top': '4px'
		});
		clear_link.css({
			display:'inline-block',
			'font-size':'9px',
			'margin-top': '4px'
		});
	});
	
	/* on clicking at any portion of document the filter section will be closed*/
	$(document).mouseup(function (e){
	    var container = $("#products_grid_before");
	    if (!container.is(e.target) && container.has(e.target).length === 0){
	    	/*if( $("#products_grid_before").css('width') == '300px') {*/
				$("#products_grid_before").css({"width":"0px","transition":"0.5s","padding-left":"0%"});
				$("#wrapwrap").css({"margin-left":"0px","transition":"0.5s"});
				$(".transparent").css("display","none");
				
			/*}		    */
	    }
	});
	$(".common-close-btn").click(function(){
		$("#products_grid_before").css({"width":"0px","transition":"0.5s","padding-left":"0%"});
		$("#wrapwrap").css({"margin-left":"0px","transition":"0.5s"});
		$(".transparent").css("display","none");
		
	})
	 $(document).on( 'keydown', function(e){
		if(e.keyCode === 27) {
			$("#products_grid_before").css({"width":"0px","transition":"0.5s","padding-left":"0%"});
			$("#wrapwrap").css({"margin-left":"0px","transition":"0.5s"});
			$(".transparent").css("display","none");
			
		}
	 })
	
	//Click to active color attribute
	$(".css_attribute_color input").change(function(){
	        $(this).parent().toggleClass('active');
	});
	
	if($(window).width() < 900) {
		$("#products_grid_before").removeClass("main_left_grid_before");
		$("#products_grid_before").removeClass("main_right_grid_before");
		$("#products_grid_before").removeClass("main_listid_grid_before");
	}
		
	// Show clear all link when any attribute is selected
	var $new_class_variant = $(".clear-all-variant")
	if($new_class_variant){
		$(".clear-all-variant").click(function(){
			var self=$(this)
			var curent_div = $(self).closest("li");
			$(curent_div).find("input:checked").each(function(){
				$(this).removeAttr("checked");
			});
			$("form.js_attributes input").closest("form").submit();
		});
	}
	
	/* changing the sequence of clikcked checkbox of attribute to first level*/
	$("form.js_attributes input:checked").each(function(){ 
		var self=$(this)        	
		var curr_parent=self.closest("li");        	
		var curr_att=curr_parent.closest("ul").find("li").first(); 
		$(curr_att).before(curr_parent);
		
		var curr_parent_color=self.closest("div");        	
		var curr_att_color=curr_parent.closest("li").find("div.color-div").first(); 
		$(curr_att_color).before(curr_parent_color);
	});
		
	// for show tab
	var p_c=$(".p_Count").attr("data-id");
	if(p_c<20)
	{  
		$(".filter-show").addClass("show_inactive")
	}
	$(".ppg_show").click(function()
	{
		var show_ppg=$(this).attr("data-id");
		var url =document.URL
		if((url.indexOf("ppg=16") >= 0))
		{
				var url = url.replace("ppg=16", "");
		}
		else if((url.indexOf("ppg=20") >= 0))
		{
				var url = url.replace("ppg=20", "");
		}
		if (url.indexOf('?') == -1)
			window.location.href = url+"?ppg="+show_ppg;
		if (!(url.indexOf('?') == -1))
			window.location.href = url+"&ppg="+show_ppg;
	});
	
	
	/*$(".alternate-allow").mouseenter(function(){
		if($(this).find("a span").hasClass('alternate-image')){
			$(this).find(".product-original-image").css({"display":"none","transition":"all 0.8s ease 0s","transform":"rotate(0deg)","z-index":"1"});
			$(this).find(".alternate-image").css({"display":"block","transition":"all 0.8s ease 0s","transform":"rotate(180deg)"});
		}
	})
	$(".alternate-allow").mouseleave(function(){
		$(this).find(".product-original-image").css({"display":"block","transition":"all 0.8s ease 0s"});
		$(this).find(".alternate-image").css({"display":"none","transition":"all 0.8s ease 0s"});
	})*/
	
	$('.nav-selection-div .sub_ctg a').each(function(){
	    var breadWidth = $(this).width();
	    if($(this).parent('.sub_ctg').hasClass('active') || $(this).parent('.sub_ctg').hasClass('first')){

	    } else {
	        $(this).css('max-width', 105 + 'px');
	        $(this).css('text-overflow','ellipsis');

	        $(this).mouseover(function(){
	            $(this).css('max-width', breadWidth + 'px');
	            $(this).css('text-overflow','unset');
	        });

	        $(this).mouseout(function(){
	            $(this).css('max-width', 105 + 'px');
	            $(this).css('text-overflow','ellipsis');
	        });
	    }
	});
	
	//Remove hover effect when wishlist, compare, cart and quickview inactive
	if(($("#wishlist_shop").hasClass("customize-deny-common")) && ($("#quick_view_shop").hasClass("customize-deny-common")) && ($("#see_similar_product").hasClass("customize-deny-common")) && ($("#compare_shop").hasClass("customize-deny-common")) && (!$("#addtocart_shop").hasClass("show_add_to_cart"))){
		$(".product-des").css("border-bottom","none");
	}
});


