import odoo
from odoo import http
from odoo.http import request
import json
import math
from odoo.addons.base.ir.ir_qweb.fields import nl2br
#from odoo.addons.website.models.website import slug
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website_form.controllers.main import WebsiteForm
from operator import pos

PPG = 20  # Products Per Page
PPR = 4   # Products Per Row


class WebsiteSaleForm(WebsiteForm):

    @http.route('/website_form/shop.sale.order', type='http', auth="public", methods=['POST'], website=True)
    def website_form_saleorder(self, **kwargs):
        model_record = request.env.ref('sale.model_sale_order')
        try:
            data = self.extract_data(model_record, kwargs)
        except ValidationError as e:
            return json.dumps({'error_fields': e.args[0]})

        order = request.website.sale_get_order()
        if data['record']:
            order.write(data['record'])

        if data['custom']:
            values = {
                'body': nl2br(data['custom']),
                'model': 'sale.order',
                'message_type': 'comment',
                'no_auto_thread': False,
                'res_id': order.id,
            }
            request.env['mail.message'].sudo().create(values)

        if data['attachments']:
            self.insert_attachment(model_record, order.id, data['attachments'])

        return json.dumps({'id': order.id})

class infigoShop(http.Controller):
    def _get_compute_currency_and_context(self):
        pricelist_context = dict(request.env.context)
        pricelist = False
        if not pricelist_context.get('pricelist'):
            pricelist = request.website.get_current_pricelist()
            pricelist_context['pricelist'] = pricelist.id
        else:
            pricelist = request.env['product.pricelist'].browse(pricelist_context['pricelist'])

        from_currency = request.env.user.company_id.currency_id
        to_currency = pricelist.currency_id
        compute_currency = lambda price: from_currency.compute(price, to_currency)

        return compute_currency, pricelist_context, pricelist

    def get_attribute_value_ids(self, product):
        product = product.with_context(quantity=1)

        visible_attrs_ids = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped('attribute_id').ids
        to_currency = request.website.get_current_pricelist().currency_id
        attribute_value_ids = []
        for variant in product.product_variant_ids:
            if to_currency != product.currency_id:
                price = variant.currency_id.compute(variant.website_public_price, to_currency)
            else:
                price = variant.website_public_price
            visible_attribute_ids = [v.id for v in variant.attribute_value_ids if v.attribute_id.id in visible_attrs_ids]
            attribute_value_ids.append([variant.id, visible_attribute_ids, variant.website_price, price])
        return attribute_value_ids

    def _get_search_order(self, post):
        return 'website_published desc,%s , id desc' % post.get('order', 'website_sequence desc')

    def _get_search_domain(self, search, category, attrib_values, price_vals=None):
        domain = request.website.sale_product_domain()
        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|',('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]
 
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
 
        if price_vals :
            domain += [('list_price','>=',price_vals.get('min_val')),('list_price','<=',price_vals.get('max_val'))]
        
        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]   
        return domain

    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        
        if 'ppg' in request.httprequest.args:
            ppg = request.httprequest.args['ppg']
        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG
            
        module_json = request.env['ir.module.module'].sudo().search([("name","=","infigo_shop_load_products"),("state","=","installed")])
        if module_json : 
            ppg = 8
        
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attributes_ids = {v[0] for v in attrib_values}
        #attrib_set = {v[1] for v in attrib_values}
        attrib_set = {v[1] if v[0] != 0 else 0 for v in attrib_values}
        brand_set = {v[1] if v[0] == 0 else 0 for v in attrib_values}
         
        request.cr.execute( 'select min(list_price),max(list_price) from product_template where sale_ok=True and active=True and website_published=True')
        min_max_vals = request.cr.fetchall()
        min_val = min_max_vals[0][0] or 0
        if int(min_val) == 0:
            min_val = 1
        max_val = min_max_vals[0][1] or 1
                    
        product_price_search_vals = {}
        attrib_price=post.get("attrib_price",'%s-%s'%(min_val,max_val))
        
        product_price_search_vals.update({'min_val':attrib_price.split("-")[0],'max_val':attrib_price.split("-")[1]})       
        domain = self._get_search_domain(search, category,  attrib_values , product_price_search_vals)
         
        min_max_price='%s-%s'%(min_val,max_val)
        if attrib_price == min_max_price:
            attrib_price=""
	    #if post["attrib_price"]:            
		    #del post["attrib_price"]
        module_navbtn = request.env['ir.module.module'].sudo().search([("name","=","infigo_navigation"),("state","=","installed")])
        if module_navbtn:
            keep = QueryURL('/shop', category=category and int(category),ept=True, search=search, attrib=attrib_list, order=post.get('order'))
        else:
            keep = QueryURL('/shop', category=category and int(category),search=search, attrib=attrib_list, order=post.get('order'))
        #keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list, order=post.get('order'))
        pricelist_context = dict(request.env.context)
        if not pricelist_context.get('pricelist'):
            pricelist = request.website.get_current_pricelist()
            pricelist_context['pricelist'] = pricelist.id
        else:
            pricelist = request.env['product.pricelist'].browse(pricelist_context['pricelist'])

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)

        url = "/shop"
        if search:
            post["search"] = search
        if category:
            category = request.env['product.public.category'].browse(int(category))
            url = "/shop/category/%s" % slug(category)
        if attrib_list:
            post['attrib'] = attrib_list

        categs = request.env['product.public.category'].search([('parent_id', '=', False)])
        Product = request.env['product.template']

        parent_category_ids = []
        if category:
            parent_category_ids = [category.id]
            current_category = category
            while current_category.parent_id:
                parent_category_ids.append(current_category.parent_id.id)
                current_category = current_category.parent_id
               
        
        product_count = Product.search_count(domain)
        pager = request.website.pager(url=url, total=product_count, page=page, step=ppg, scope=7, url_args=post)
        products = Product.search(domain, limit=ppg, offset=pager['offset'], order=self._get_search_order(post))
        all_product = Product.search(domain,order=self._get_search_order(post))
        products_count = Product.search_count(domain)
        
        total_page = math.ceil(product_count / 8) 
        
        ProductAttribute = request.env['product.attribute']
        if products:
            attributes = ProductAttribute.search([('attribute_line_ids.product_tmpl_id', 'in', products.ids)])
        else:
            attributes = ProductAttribute.browse(attributes_ids)

        from_currency = request.env.user.company_id.currency_id
        to_currency = pricelist.currency_id
        compute_currency = lambda price: from_currency.compute(price, to_currency)
        
        # To load the script base on module install
        module = request.env['ir.module.module'].sudo().search([("name","=","website_sale_options"),("state","=","installed")])
        if module : 
            script = "/website_sale_options/static/src/js/website_sale.js"
        else : 
            script = "/infigo_shop_load_products/static/src/js/website_sale_json.js"

    
        values = {
            'search': search,
            'category': category,
            
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'brand_set':brand_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'search_count': product_count,  # common for all searchbox
            'rows': PPR,
            'remain_count':len(products),
            'domain':domain,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'parent_category_ids': parent_category_ids,
            'all_product':all_product,
            'total_page':total_page,
            'oe_script':script,
            'products_count':products_count,
            'pricefilter':True,
            'order':post.get('order',None),
        }
        if category:
            values['main_object'] = category
        return request.render("website_sale.products", values)  


