$(document).ready(function(){
	
	if($('div').hasClass('oe_list'))
		{
		$("div").find(".main").addClass("main_list");
		$("div").find("#products_grid").addClass("main_listid");
		$("div").find("#products_grid_before").addClass("main_listid_grid_before");
		$("div").find(".right-cnt-maxW").addClass("right-cnt-maxW_list");
		$("div").find(".shop-container").addClass("block");
		$("div").find(".shop-container").removeClass("shop-container");
		$("div").find(".menu_view_filter_div").addClass("left_side_bar_view_filter");
		$("div").find(".product-name-h5").addClass("product-name-h5_list");
		$("div").find(".product-des").addClass("product-des_list");
		$('.menu-filter').css("display","none");
		$('.filter_block').removeClass('mCustomScrollbar').removeAttr('data-mcs-theme');
		if ($(window).width() > 900) {
			$('#products_grid_before').removeClass('mCustomScrollbar').removeAttr('data-mcs-theme');
		}
		
		$(".filter_right_ul").removeClass("mCustomScrollbar");
		$('#image_show,#text_allignment').css("display","none");
		$(".shop-breadcrumb").css({'max-width':'1400px','width':'90%'});
	}
	//screen 900 to hide filter
	
	if ($(window).width() < 900) {
		$('.menu-filter').css("display","block");
		$('.main_listid').css("cssText", "width: 100% !important;");
		$("div").find(".menu_view_filter_div").removeClass("left_side_bar_view_filter");
		$("#products_grid_before").removeClass("main_listid_grid_before");
	}else{
		
	}
});

/*$(window).load(function(){
	var filter_label = $('.main_list').parent().find('.view-as-div').css("margin-left","0px");
	var filter_label = $('.main_list').parent().find('label.view-label').addClass('filter-label-alignment');
})*/

