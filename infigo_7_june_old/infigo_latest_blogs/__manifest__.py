{
    # Theme information
    'name' : 'Infigo Latest Blogs',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Drag and Drop Latest Blog Caraousel anywhere in Website',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_blog',
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
