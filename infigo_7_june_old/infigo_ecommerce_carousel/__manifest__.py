{
    # Theme information
    'name' : 'Infigo Ecommerce Carousel',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contains Product and Multi Product Carousels',
    'description': """""",

    # Dependencies
    'depends': [
    
	    'infigo_product_multi_carousel',

	
    ],


    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}

