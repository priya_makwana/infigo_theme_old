{
    # Theme information
    'name' : 'Infigo Business Carousel',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contains Employee,Customer and Portfolio Carousels',
    'description': """""",

    # Dependencies
    'depends': [
	    'infigo_employee_carousel',
	    'infigo_customer_carousel',
	    'infigo_gallery'
    ],


    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}

