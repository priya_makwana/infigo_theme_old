{
    # Theme information
    'name' : 'Infigo Landing Page',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Professionally Designed Unique Landing Page Layout',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_business_cms_blocks','infigo_ecommerce_cms_blocks'
    ],

    # Views
    'data': [
        'templates/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
