{
    # Theme information
    'name' : 'Infigo Gallery',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Configure Portflio Gallery at backend and Drag & Drop Portfolio Snippet anywhere in Website',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_carousel','infigo_business_snippets'
    ],

    # Views
    'data': [
        'security/ir.model.access.csv',	
        'templates/assets.xml',
        'templates/gallery_option.xml',
        'templates/gallery_carousel.xml',
        'views/portfolio_view.xml',
        'views/portfolio_category_view.xml',
        'views/multi_img.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
