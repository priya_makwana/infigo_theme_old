{
    # Theme information
    'name' : 'Infigo Customize Quick View',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Quick View Custom Style Attribute for Customize shop page',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_quick_view','infigo_shop_customize'
    ],

    # Views
    'data': [        
        'templates/template.xml',
       
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
