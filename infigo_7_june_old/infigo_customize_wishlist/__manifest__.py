{
    # Theme information
    'name' : 'Infigo Customize Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Wishlist Custom Style Attribute for Customize shop page',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_wishlist','infigo_shop_customize'
    ],

    # Views
    'data': [        
        'templates/template.xml',
       
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
