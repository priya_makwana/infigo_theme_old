{
    # Theme information
    'name' : 'Infigo Product Carousel Compare',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Compare from Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_product_multi_carousel','infigo_compare'
    ],

    # Views
    'data': [
        'templates/template.xml', 
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
