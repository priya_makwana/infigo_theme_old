from odoo import api, fields, models
    
    
class brand(models.Model):
    _name="product.brand"
    
    name=fields.Char("Brand Name",required=True)
    is_website_published=fields.Boolean("Website Published")
