{
    # Theme information
    'name' : 'Infigo Events',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Easy & User Friendly Event Platform for your Online Store',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_layout','website_event'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
