{
    # Theme information
    'name' : 'Infigo Customize Compare',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Compare Custom Style Attribute for Customize shop page',
    'description': """""",

    # Dependencies
    'depends': [
       'infigo_compare','infigo_shop_customize'
    ],

    # Views
    'data': [        
        'templates/template.xml',
       
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',

    # Technical
    'installable': True,
    'auto_install': False,
}
