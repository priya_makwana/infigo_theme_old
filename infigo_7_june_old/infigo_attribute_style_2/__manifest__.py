{
    # Theme information
    'name' : 'Infigo Attribute Style 2',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Select Multiple Attribute Options and apply for Search in One time at Category Page',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_pricefilter'
    ],

    # Views
    'data': [
       	
        'templates/assets.xml',
        'templates/template.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
