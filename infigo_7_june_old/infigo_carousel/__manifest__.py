{
    # Theme information
    'name' : 'Infigo Carousel Editor',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Carousel RTE Editor',
    'description': """""",

    # Dependencies
    'depends': [
		'infigo_layout'
    ],

    # Views
    'data': [
        'templates/assets.xml',
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
