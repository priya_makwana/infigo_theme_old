{
    # Theme information
    'name' : 'Infigo Product Carousel Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Wishlist from Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'infigo_product_multi_carousel','infigo_wishlist'
    ],

    # Views
    'data': [
        'templates/template.xml', 
    ],

    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',

    # Technical
    'installable': True,
}
